<?php

namespace Infotechnohelp\Proxy;

class Proxy
{
    private static function getRandomUserAgent()
    {
        $userAgents = UserAgentList::getUserAgentList();

        $i = rand(0, count($userAgents) - 1);

        return $userAgents[$i];
    }

    public static function sendRequest(string $url, string $proxyTcp, bool $throwException = true, bool $useRandomUserAgent = false)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxyTcp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        #curl_setopt($ch, CURLOPT_HEADER, 1); // Not required
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        if ($useRandomUserAgent) {
            curl_setopt($ch, CURLOPT_USERAGENT, self::getRandomUserAgent());
        }

        try {
            $contents = curl_exec($ch);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }


        if ($contents === false) {
            throw new \Exception(curl_error($ch), curl_errno($ch));
        }

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($httpCode === 403) {

            if ($throwException) {
                throw new \Exception("CURL response 403: $proxyTcp $url", 403);
            }

            return false;
        }

        return $contents;
    }

    // @todo add paramater int repeat
    
    public static function forcedResponse(string $url, string $proxyTcp, bool $useRandomUserAgent = false)
    {
        $responseReceived = false;

        $html = null;

        while (!$responseReceived) {
            try {
                $html = Proxy::sendRequest($url, $proxyTcp, false, $useRandomUserAgent);
                $responseReceived = true;
            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }
        
        return $html;
    }

    public static function filterWorkingProxies(array $tcpList, $url): array
    {
        $result = [];

        foreach ($tcpList as $tcp) {

            $time_pre = microtime(true);

            $exceptionCatched = false;

            try {
                self::sendRequest($url, $tcp, true, true);
            } catch (\Exception $e) {
                $exceptionCatched = true;
            }


            $time_post = microtime(true);
            $exec_time = $time_post - $time_pre;

            echo "$tcp";

            if (!$exceptionCatched && $exec_time <= 10) {
                $result[] = $tcp;

                echo " OK";
            }

            echo "\n";
        }

        return $result;
    }
}